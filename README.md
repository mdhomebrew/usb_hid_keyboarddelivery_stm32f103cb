# README #

This project builds an ARM project targeted at a STM32F103CB microcontroller. 

The project binary is set up to be loaded at a specific location in the binary extracted from a SteelSeries Sensei gaming mouse which uses the same microcontroller.

Slides from our DEF CON 25 presentation available here: https://blackstargroup.org/DEFCON-25-skud-and-Sky_IfYouGiveAMouseAMicrochip.pdf 
_Follow the slides for details on how to recreate this hardware hack at home!

The end result:

* Plug mouse in
* Inserted application is a usb keyboard HID,types out an exutable batch file, saves it to %temp%, and executes it from the run menu using keyboard and mouse commands
* Inserted application branches back to the original firmware on the mouse, and the mouse works normally.

The application is a Chocolate DOOM game cheat that uses keyboard shortcuts to enable different options:

* Alt + +: increase armor
* Alt + 0: unlimited ammo
* Alt + [number]: unlock weapon tied to that slot - allows access to the BFG, Plasma Cannon, etc on the first level of DOOM for all you noobs who are struggling with it.

### What is this repository for? ###

* USB Keyboard keystroke payload delivery designed for inserting into the binary extracted from a STM32F103CB based USB device.
* This specifically targets a SteelSeries Sensei mouse and uses it to deliver and execute a game cheat for the original DOOM.

### Tools needed ###

* http://www.openstm32.org/System+Workbench+for+STM32
* http://www.st.com/en/development-tools/st-link-v2.html
* http://www.st.com/en/embedded-software/stm32cube-embedded-software.html
* http://www.st.com/en/embedded-software/stm32cubef1.html